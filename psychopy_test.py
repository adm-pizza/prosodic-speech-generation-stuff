
from psychopy import visual, core, event, logging  # import some libraries from PsychoPy

alphanumerics = [""]

logging.console.setLevel(logging.INFO)
#create a window
mywin = visual.Window([1300,800], fullscr=True, monitor="testMonitor", units="height")

mov = visual.MovieStim3(mywin, filename='/home/rail/Documents/stt/v3/file_assembly/videos-static/exp-m-01.mp4', pos=(0,+100))
text = visual.TextStim(mywin, text="|", pos=(0, -0.3), height=0.05, autoLog=False)
mov.draw()
mov.autoDraw = True
mov.play()
mywin.flip()
text.draw()

shift = False
continueRoutine = True
addString=""

# String entry. Complicated, but works great.
while continueRoutine:

    keys = event.getKeys()
    if len(keys):
        print(keys)
        if 'space' in keys:
            addString=" "
        elif 'backspace' in keys:
            splitText = text.text.split("|")
            if len(splitText[0])>0:
                splitText[0] = splitText[0][:-1]
            text.text = "|".join(splitText)
        elif 'delete' in keys:
            splitText = text.text.split("|")
            if len(splitText[1])>0:
                splitText[1] = splitText[1][1:]
            text.text = "|".join(splitText)
        elif 'lshift' in keys or 'rshift' in keys:
            shift = True
        elif 'return' in keys:
            continueRoutine = False
        elif 'left' in keys:
            splitText = text.text.split("|")
            if len(splitText[0])>0:
                movedChar = splitText[0][-1]
                splitText[0] = splitText[0][:-1]
                text.text = ("|"+movedChar).join(splitText)
        elif 'right' in keys:
            splitText = text.text.split("|")
            if len(splitText[1])>0:
                movedChar = splitText[1][0]
                splitText[1] = splitText[1][1:]
                text.text = (movedChar+"|").join(splitText)
        elif 'comma' in keys:
            addString=","
        elif 'period' in keys:
            addString=","
        elif len(keys[0])==1 and keys[0].isalpha():
            if shift:
                addString=keys[0].upper()
                shift = False
            else:
                addString=keys[0]

    if len(addString)>0:
        text.text = text.text.replace('|', addString+'|')
        addString=""

    text.draw()
    mywin.flip()

#mov.pause()

while True:
    text.flip()
    mywin.flip()
    if len(event.getKeys())>0:
        mywin.close()
        core.quit()

mywin.close()
core.quit()
